package com.aholddelhaize.main.core.drivers;

import com.aholddelhaize.main.core.utils.PropertiesReader;
import com.aholddelhaize.main.core.utils.PropertiesUtils;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class BasePLSQLDriver {

    public static String DATABASE_URL;
    public static String USER;
    public static String PASSWD;
    public Connection conn = null;
    public Properties config;
    public PropertiesReader propertiesReader;

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, Object[] testArguments)
            throws ClassNotFoundException {
        config = PropertiesUtils.getProcessedTestProperties();
        propertiesReader = new PropertiesReader(config);
        DATABASE_URL = propertiesReader.getDatabaseUrl();
        USER = propertiesReader.getUser();
        PASSWD = propertiesReader.getPasswd();
        Class.forName("oracle.jdbc.driver.OracleDriver");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(Method method, ITestResult result) throws SQLException {
        conn.close();
    }
}
