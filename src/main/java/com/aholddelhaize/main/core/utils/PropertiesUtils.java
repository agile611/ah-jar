package com.aholddelhaize.main.core.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertiesUtils {
    public static final String DATABASE_URL = "database_url";
    public static final String USER = "user";
    public static final String PASSWD = "passwd";

    /**
     * Loads a properties file using namespace as file name. If the file is not found returns an empty properties file.
     *
     * @return Properties read from the file
     */
    public static Properties loadResourceProperties() {
        Properties prop = new Properties();
        try {
            prop.load(new InputStreamReader(PropertiesUtils.class.getClassLoader()
                    .getResourceAsStream("config.properties")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    /**
     * There are two levels for properties: system properties (command-line) with higher preference and project-specific, with lower preference.
     *
     * @param resourceProperties the properties to be overriden with system properties
     * @param systemProperties   the properties to override
     */
    public static void overrideProperties(Properties resourceProperties,
                                          Properties systemProperties) {
        if (resourceProperties != null) {
            for (Object keyObject : resourceProperties.keySet()) {
                String key = (String) keyObject;
                if (systemProperties.containsKey(key)) {
                    resourceProperties.setProperty(key, systemProperties.getProperty(key));
                }
            }
        }
    }

    /**
     * Returns the properties overridden with system (command-line) properties.
     *
     * @return the properties overridden with system (command-line) properties.
     */
    public static Properties getProcessedTestProperties() {
        Properties resourceProperties = loadResourceProperties();
        Properties systemProperties = System.getProperties();
        overrideProperties(resourceProperties, systemProperties);
        return resourceProperties;
    }
}
