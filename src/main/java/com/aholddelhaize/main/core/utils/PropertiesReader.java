package com.aholddelhaize.main.core.utils;

import java.util.Properties;

/**
 * Handle Suites configuration in the framework with its properties.
 *
 * @author guillem.hernandez
 */
public class PropertiesReader {

    public static String EMAILS;
    public static String TIMEOUT;
    public static String NUM_THREADS;
    public static String DATA_PROVIDER_THREAD_COUNT;
    public static String HAS_RETRY;
    public static String RETRIES;
    public static String SUITE_NAME;
    public static String REPORTS_DIR;
    public static String TEST_PACKAGE;
    public static String DATABASE_URL;
    public static String USER;
    public static String PASSWD;

    public PropertiesReader(Properties props) {
        SUITE_NAME = props.getProperty("suite");
        TEST_PACKAGE = props.getProperty("test_package", "unset");
        EMAILS = props.getProperty("emails", "unset");
        TIMEOUT = props.getProperty("timeout", "unset");
        NUM_THREADS = props.getProperty("num_of_threads", "unset");
        DATA_PROVIDER_THREAD_COUNT = props.getProperty("data_provider_thread_count", "unset");
        HAS_RETRY = props.getProperty("has_retry", "unset");
        RETRIES = props.getProperty("retries", "unset");
        REPORTS_DIR = props.getProperty("reports_dir");
        DATABASE_URL = props.getProperty("database_url", "unset");
        USER = props.getProperty("user", "unset");
        PASSWD = props.getProperty("passwd", "unset");
    }

    public static int getTimeout() {
        return Integer.parseInt(TIMEOUT.replaceAll("\\s+", ""));
    }

    public static int getNumThreads() {
        return Integer.parseInt(NUM_THREADS.replaceAll("\\s+", ""));
    }

    public static int getNumDataProviderThreads() {
        return Integer.parseInt(DATA_PROVIDER_THREAD_COUNT.replaceAll("\\s+", ""));
    }

    public static boolean hasRetry() {
        return Boolean.parseBoolean(HAS_RETRY.replaceAll("\\s+", ""));
    }

    public static String getReportsDir() {
        return REPORTS_DIR.replaceAll("\\s+", "");
    }

    public static String getSuiteName() {
        return SUITE_NAME.replaceAll("\\s+", "");
    }

    public String getStartUrl() {
        return SUITE_NAME.replaceAll("\\s+", "");
    }

    public String getDatabaseUrl() {
        return DATABASE_URL.replaceAll("\\s+", "");
    }

    public String getUser() {
        return USER.replaceAll("\\s+", "");
    }

    public String getPasswd() {
        return PASSWD.replaceAll("\\s+", "");
    }
}