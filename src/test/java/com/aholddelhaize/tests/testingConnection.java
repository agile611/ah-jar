package com.aholddelhaize.tests;

import com.aholddelhaize.main.core.drivers.BasePLSQLDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.DriverManager;

/**
 * @author ghernandez
 */
public class testingConnection extends BasePLSQLDriver {

    @Test
    public void testingConnection() {
        try {
            System.out.println("Connecting to database...");
            System.out.println("Database ..." + DATABASE_URL);
            System.out.println("User ..." + USER);
            System.out.println("Passwd ..." + PASSWD);
            conn = DriverManager.getConnection(DATABASE_URL, USER, PASSWD);
            Assert.assertTrue(conn.isValid(10));// 10 sec
            System.out.println("Connected to database...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
